//
//  main.m
//  ScrumPointer
//
//  Created by Scott Hammer on 3/12/12.
//  Copyright (c) 2012 Hammer Consulting LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCAppDelegate class]));
    }
}
