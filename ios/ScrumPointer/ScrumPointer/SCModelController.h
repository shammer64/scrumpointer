//
//  SCModelController.h
//  ScrumPointer
//
//  Created by Scott Hammer on 3/12/12.
//  Copyright (c) 2012 Hammer Consulting LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SCDataViewController;

@interface SCModelController : NSObject <UIPageViewControllerDataSource>
- (SCDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(SCDataViewController *)viewController;
@end
