//
//  SCRootViewController.h
//  ScrumPointer
//
//  Created by Scott Hammer on 3/12/12.
//  Copyright (c) 2012 Hammer Consulting LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCRootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
