//
//  SCAppDelegate.h
//  ScrumPointer
//
//  Created by Scott Hammer on 3/12/12.
//  Copyright (c) 2012 Hammer Consulting LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
