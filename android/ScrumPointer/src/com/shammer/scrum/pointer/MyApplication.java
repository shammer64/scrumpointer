package com.shammer.scrum.pointer;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MyApplication extends Application {

	private String pointScale;
	private int pointSelectionIndex;

	@Override
	public void onCreate() {
		loadPrefs();
		super.onCreate();
	}

	@Override
	public void onTerminate() {
		storePrefs();
		super.onTerminate();
	}

	private void loadPrefs() {
		SharedPreferences prefs = getSharedPreferences("com.shammer.scrum.pointer", 0);
		this.pointScale = prefs.getString("pointScalePref", "fibo");
	}

	private void storePrefs() {
		SharedPreferences prefs = getSharedPreferences("com.shammer.scrum.pointer", 0);
		Editor editor = prefs.edit();
		editor.putString("pointScalePref", getPointScale());
		editor.commit();
	}

	public void setPointScale(String pointScale) {
		this.pointScale = pointScale;
		storePrefs();
	}

	public String getPointScale() {
		loadPrefs();
		return pointScale;
	}

	public void setPointSelectionIndex(int pointSelectionIndex) {
		this.pointSelectionIndex = pointSelectionIndex;
	}

	public int getPointSelectionIndex() {
		return pointSelectionIndex;
	}

}
