package com.shammer.scrum.pointer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class PrefsActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getPreferenceManager().setSharedPreferencesName("com.shammer.scrum.pointer");
		addPreferencesFromResource(R.xml.preferences);
	}

}
