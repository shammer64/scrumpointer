package com.shammer.scrum.pointer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class SelectActivity extends Activity {

	public static final String[] scaleFibo = { "\u00BD", "1", "2", "3", "5", "8", "13", "21" };
	public static final String[] scaleDouble = { "\u00BD", "1", "2", "4", "8", "16", "32", "64" };
	private GridView gridView;
	private String pointScale;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select);

		gridView = (GridView) findViewById(R.id.gridView);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				MyApplication myApp = (MyApplication) getApplication();
				myApp.setPointSelectionIndex((int) id);
				Intent i = new Intent(SelectActivity.this, PointActivity.class);
				startActivity(i);
			}
		});
	}

	@Override
	protected void onStart() {
		MyApplication myApp = (MyApplication) getApplication();
		pointScale = myApp.getPointScale();
		if ("fibo".equals(pointScale)) {
			gridView.setAdapter(new GridArrayAdapter(this, R.layout.grid_item, scaleFibo));
		} else {
			gridView.setAdapter(new GridArrayAdapter(this, R.layout.grid_item, scaleDouble));
		}
		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		menu.add(0, 0, 0, "Preferences").setIcon(android.R.drawable.ic_menu_preferences);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent i = new Intent(this, PrefsActivity.class);
			startActivity(i);
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	class GridArrayAdapter extends ArrayAdapter<String> {
		private int currentHeight = 0;
		private int currentWidth = 0;
		private final int rotation;

		public GridArrayAdapter(Context context, int textViewResourceId, String[] objects) {
			super(context, textViewResourceId, objects);
			WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
			DisplayMetrics metrics = new DisplayMetrics();
			wm.getDefaultDisplay().getMetrics(metrics);
			this.currentHeight = metrics.heightPixels;
			this.currentWidth = metrics.widthPixels;
			this.rotation = wm.getDefaultDisplay().getRotation();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.grid_item, null);
				float pixels = 0;
				if (this.rotation % 2 == 0) {
					pixels = (this.currentHeight / 4) * ((float) this.currentWidth / (float) this.currentHeight);
				} else {
					pixels = (this.currentWidth / 4) * (this.currentHeight / this.currentWidth);
				}
				((TextView) convertView).setTextSize(TypedValue.COMPLEX_UNIT_PX, pixels);
			}
			return super.getView(position, convertView, parent);
		}
	}

}