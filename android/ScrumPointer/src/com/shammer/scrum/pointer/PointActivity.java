package com.shammer.scrum.pointer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.TextView;

public class PointActivity extends Activity {

	private TextView pointView;
	private GestureDetector gestureDetector;
	private String pointScale;
	private String[] pointScaleValues;
	private int selectedIdx;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.point);

		pointView = (TextView) findViewById(R.id.pointView);
		gestureDetector = new GestureDetector(this, new MyGestureDetector());
	}

	@Override
	protected void onStart() {
		MyApplication myApp = (MyApplication) getApplication();
		pointScale = myApp.getPointScale();
		if ("fibo".equals(pointScale)) {
			pointScaleValues = SelectActivity.scaleFibo;
		} else {
			pointScaleValues = SelectActivity.scaleDouble;
		}
		selectedIdx = myApp.getPointSelectionIndex();
		updatePointValue();
		super.onStart();
	}

	protected void updatePointValue() {
		String selectedText = pointScaleValues[selectedIdx];
		pointView.setText(selectedText);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gestureDetector.onTouchEvent(event))
			return true;
		else
			return super.onTouchEvent(event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		menu.add(0, 0, 0, "Preferences").setIcon(android.R.drawable.ic_menu_preferences);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent i = new Intent(this, PrefsActivity.class);
			startActivity(i);
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	class MyGestureDetector extends SimpleOnGestureListener {
		private static final int SWIPE_MIN_DISTANCE = 60;
		@SuppressWarnings("unused")
		private static final int SWIPE_MAX_OFF_PATH = 250;
		private static final int SWIPE_THRESHOLD_VELOCITY = 100;

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			MyApplication myApp = (MyApplication) getApplication();
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				// Swipe left
				selectedIdx = (++selectedIdx % pointScaleValues.length);
			} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				// Swipe right
				selectedIdx = ((--selectedIdx < 0) ? pointScaleValues.length - 1 : selectedIdx);
			}
			myApp.setPointSelectionIndex(selectedIdx);
			updatePointValue();
			return true;
		}
	}
}